class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  def hello_action 
    render html: "Hello World!"
  end
  
end
